package com.service.safeservice

import com.service.safeservice.Common.CyclicArray
import org.junit.Test

import org.junit.Assert.*
import org.junit.jupiter.api.DynamicTest
import org.junit.runner.RunWith
import org.junit.jupiter.api.TestFactory

class CyclicArrayTest {
    @TestFactory
    fun cyclicArrayAdd() = listOf(
            Triple(10, listOf(1.0f, -1.5f, 30.8f), 3),
            Triple(3, listOf(1.0f, -1.5f, 30.8f), 3),
            Triple(2, listOf(1.0f, -1.5f, 30.8f), 2)
        ).map { (maxSize: Int, pushed: List<Float>, expected: Int) ->
            DynamicTest.dynamicTest("CyclicArray<Float>OfMaxSize${maxSize}_WhenAdd${pushed.size}Elements_HasSize${expected}") {
                var array: CyclicArray<Float> = CyclicArray<Float>(maxSize)
                for (item in pushed) {
                    array.push(item)
                }
                assert(array.size == expected)
            }
        }

    @TestFactory
    fun cyclicArrayIterating() = listOf(
        Triple(10, listOf(1.0f, -1.5f, 30.8f), listOf(1.0f, -1.5f, 30.8f)),
        Triple(3, listOf(1.0f, -1.5f, 30.8f), listOf(1.0f, -1.5f, 30.8f)),
        Triple(2, listOf(1.0f, -1.5f, 30.8f), listOf(-1.5f, 30.8f)),
        Triple(10, listOf(1.0f, -1.5f, 30.8f, 5.0f, -6.0f), listOf(1.0f, -1.5f, 30.8f, 5.0f, -6.0f)),
        Triple(3, listOf(1.0f, -1.5f, 30.8f, 5.0f, -6.0f), listOf(30.8f, 5.0f, -6.0f)),
        Triple(2, listOf(1.0f, -1.5f, 30.8f, 5.0f, -6.0f), listOf(5.0f, -6.0f))
    ).map { (maxSize: Int, pushed: List<Float>, expected: List<Float>) ->
        DynamicTest.dynamicTest("CyclicArray<Float>OfMaxSize${maxSize}_WhenIteratingAfterPushing${pushed.size}Items_HasLast${maxSize}Elements") {
            var array: CyclicArray<Float> = CyclicArray<Float>(maxSize)
            for (item in pushed) {
                array.push(item)
            }
            var iterator = expected.iterator()
            for (item in array) {
                assert(iterator.hasNext())
                assert(item == iterator.next())
            }
        }
    }
}