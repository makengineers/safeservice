package com.service.safeservice.Common

data class AuthorizeGetApiResponse(val error: String?, val matching_user_probability: Float?, val profile_ready: Boolean?)