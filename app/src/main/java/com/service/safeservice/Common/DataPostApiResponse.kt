package com.service.safeservice.Common

data class DataPostApiResponse(val error: String?, val device_token: String?, val event_file: String?, val sensor_file: String?)