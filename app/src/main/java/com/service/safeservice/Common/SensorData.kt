package com.service.safeservice.Common

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.lang.RuntimeException

@Parcelize
class SensorData(var timestamp: Long, var acceleration: FloatArray, var magneticField: FloatArray, var gyroscope: FloatArray,
                 var gravity: FloatArray, var linearAcceleration: FloatArray, var rotation: FloatArray) : Parcelable {

    fun ToVector(): FloatArray {
        var result = FloatArray(
            acceleration.size + magneticField.size + gyroscope.size + gravity.size
                    + linearAcceleration.size + rotation.size
        )
        var actSize = 0
        System.arraycopy(acceleration, 0, result, actSize, acceleration.size)
        actSize += acceleration.size
        System.arraycopy(magneticField, 0, result, actSize, magneticField.size)
        actSize += magneticField.size
        System.arraycopy(gyroscope, 0, result, actSize, gyroscope.size)
        actSize += gyroscope.size
        System.arraycopy(gravity, 0, result, actSize, gravity.size)
        actSize += gravity.size
        System.arraycopy(linearAcceleration, 0, result, actSize, linearAcceleration.size)
        actSize += linearAcceleration.size
        System.arraycopy(rotation, 0, result, actSize, rotation.size)
        actSize += rotation.size
        return result
    }

    @kotlin.ExperimentalUnsignedTypes
    fun ToBytes() : UByteArray {
        var res = UByteArray(totalSize())
        val timestamp = System.currentTimeMillis()
        val convertedAccX : Int = (acceleration[0].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedAccY : Int = (acceleration[1].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedAccZ : Int = (acceleration[2].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedMagX : Int = (magneticField[0].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedMagY : Int = (magneticField[1].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedMagZ : Int = (magneticField[2].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedGyroX : Int = (gyroscope[0].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedGyroY : Int = (gyroscope[1].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedGyroZ : Int = (gyroscope[2].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedGravX : Int = (gravity[0].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedGravY : Int = (gravity[1].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedGravZ : Int = (gravity[2].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedLinAccX : Int = (linearAcceleration[0].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedLinAccY : Int = (linearAcceleration[1].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedLinAccZ : Int = (linearAcceleration[2].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedRotX : Int = (rotation[0].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedRotY : Int = (rotation[1].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        val convertedRotZ : Int = (rotation[2].toDouble() * Int.MAX_VALUE / accelerationMax).toInt()
        var actOffset : Int = 0
        ByteHelper.WriteAsUByteArrayInBE(timestamp, res, actOffset)
        actOffset += 8
        ByteHelper.WriteAsUByteArrayInBE(convertedAccX, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedAccY, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedAccZ, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedMagX, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedMagY, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedMagZ, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedGyroX, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedGyroY, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedGyroZ, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedGravX, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedGravY, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedGravZ, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedLinAccX, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedLinAccY, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedLinAccZ, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedRotX, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedRotY, res, actOffset)
        actOffset += 4
        ByteHelper.WriteAsUByteArrayInBE(convertedRotZ, res, actOffset)
        actOffset += 4
        return res
    }

    fun totalSize() : Int {
        var size = 8 + acceleration.size * 4 + magneticField.size * 4 + gyroscope.size * 4 + gravity.size *4 + linearAcceleration.size * 4 + rotation.size * 4
        return size
    }

    companion object {
        private val sensorSize: Int = 3
        val accelerationMax: Float = 32.0f
        val magneticFieldMax: Float = 500.0f
        val gyroscopeMax: Float = 100.0f
        val gravityMax: Float = 32.0f
        val linearAccelerationMax: Float = 32.0f
        val rotationMax: Float = 10.0f
        val expectedByteSize: Int = 80
        fun FromVector(timestamp: Long, vector: FloatArray) : SensorData {
            var acceleration = FloatArray(sensorSize)
            var magneticField = FloatArray(sensorSize)
            var gyroscope = FloatArray(sensorSize)
            var gravity = FloatArray(sensorSize)
            var linearAcceleration = FloatArray(sensorSize)
            var rotation = FloatArray(sensorSize)
            var actSize = 0
            System.arraycopy(vector, actSize, acceleration, 0,
                sensorSize
            )
            actSize += sensorSize
            System.arraycopy(vector, actSize, magneticField, 0,
                sensorSize
            )
            actSize += sensorSize
            System.arraycopy(vector, actSize, gyroscope, 0,
                sensorSize
            )
            actSize += sensorSize
            System.arraycopy(vector, actSize, gravity, 0,
                sensorSize
            )
            actSize += sensorSize
            System.arraycopy(vector, actSize, linearAcceleration, 0,
                sensorSize
            )
            actSize += sensorSize
            System.arraycopy(vector, actSize, rotation, 0,
                sensorSize
            )
            actSize += sensorSize
            return SensorData(
                timestamp, acceleration, magneticField, gyroscope, gravity, linearAcceleration,
                rotation
            )
        }

        @kotlin.ExperimentalUnsignedTypes
        fun FromBytes(arr: UByteArray) : SensorData {
            if(arr.size != expectedByteSize) {
                throw RuntimeException("Array of size ${arr.size} was not expected (expected $expectedByteSize")
            }

            var timestamp: Long = 0
            var acceleration: FloatArray = FloatArray(sensorSize)
            var magneticField: FloatArray = FloatArray(sensorSize)
            var gyroscope: FloatArray = FloatArray(sensorSize)
            var gravity: FloatArray = FloatArray(sensorSize)
            var linearAcceleration: FloatArray = FloatArray(sensorSize)
            var rotation: FloatArray = FloatArray(sensorSize)
            var actOffset: Int = 0
            timestamp = ByteHelper.FromUByteArrayToLongBE(arr, actOffset)
            actOffset += 8
            acceleration[0] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * accelerationMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            acceleration[1] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * accelerationMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            acceleration[2] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * accelerationMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            magneticField[0] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * magneticFieldMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            magneticField[1] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * magneticFieldMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            magneticField[2] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * magneticFieldMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            gyroscope[0] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * gyroscopeMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            gyroscope[1] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * gyroscopeMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            gyroscope[2] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * gyroscopeMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            gravity[0] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * gravityMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            gravity[1] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * gravityMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            gravity[2] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * gravityMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            linearAcceleration[0] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * linearAccelerationMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            linearAcceleration[1] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * linearAccelerationMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            linearAcceleration[2] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * linearAccelerationMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            rotation[0] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * rotationMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            rotation[1] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * rotationMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            rotation[2] = (ByteHelper.FromUByteArrayToIntBE(arr, actOffset) * rotationMax.toDouble() / Int.MAX_VALUE).toFloat()
            actOffset += 4
            return SensorData(
                timestamp,
                acceleration,
                magneticField,
                gyroscope,
                gravity,
                linearAcceleration,
                rotation
            )
        }
    }
}
