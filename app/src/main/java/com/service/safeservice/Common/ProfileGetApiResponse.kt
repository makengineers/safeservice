package com.service.safeservice.Common

data class ProfileGetApiResponse(val error: String?, val profile_ready: Boolean?, val profile: String?, val support : BooleanArray?,
                                 val creation_date: String?) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ProfileGetApiResponse

        if (error != other.error) return false
        if (profile_ready != other.profile_ready) return false
        if (profile != other.profile) return false
        if (support != null) {
            if (other.support == null) return false
            if (!support.contentEquals(other.support)) return false
        } else if (other.support != null) return false
        if (creation_date != other.creation_date) return false

        return true
    }

    override fun hashCode(): Int {
        var result = error?.hashCode() ?: 0
        result = 31 * result + (profile_ready?.hashCode() ?: 0)
        result = 31 * result + (profile?.hashCode() ?: 0)
        result = 31 * result + (support?.contentHashCode() ?: 0)
        result = 31 * result + (creation_date?.hashCode() ?: 0)
        return result
    }
}