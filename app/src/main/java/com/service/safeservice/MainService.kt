package com.service.safeservice

import android.R
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.bosphere.filelogger.FL
import com.service.safeservice.Auth.*
import com.service.safeservice.Common.AndroidInternalFileSystemManager
import com.service.safeservice.Common.AndroidNetworkUtils
import com.service.safeservice.Common.AuthType
import com.service.safeservice.CommonAbstracts.*
import com.service.safeservice.CommonAbstracts.Observable
import com.service.safeservice.Configs.AndroidUserConfigProvider
import com.service.safeservice.Common.BiometricAuthenticationAllower
import com.service.safeservice.ContinuousAuth.ContinuousAuthSwitchDetector
import com.service.safeservice.Events.AndroidEventDataGatherer
import com.service.safeservice.Events.AndroidEventDataSaver
import com.service.safeservice.Senders.AndroidApiRequester
import com.service.safeservice.Senders.AndroidDataSender
import com.service.safeservice.Sensors.AndroidSensorDataGatherer
import com.service.safeservice.Sensors.AndroidSensorDataSaver
import java.io.File
import java.util.*
import kotlin.collections.ArrayList


class MainService : Service() {
    private val myReceiver: BroadcastReceiver = MyBroadcastReceiver()
    private val screenOffReceiver: ScreenOffBroadcastReceiver = ScreenOffBroadcastReceiver()
    private val shutdownBroadcastReceiver: ShutdownBroadcastReceiver = ShutdownBroadcastReceiver(this)
    var counter = 0
    private var sensorDataGatherer: SensorDataGatherer = AndroidSensorDataGatherer(1.0f/15.0f)
    private var eventDataGatherer: EventDataGatherer = AndroidEventDataGatherer()
    private lateinit var sensorSaver: SensorDataSaver
    private lateinit var eventSaver: EventDataSaver
    private lateinit var deviceId: String
    private lateinit var dataSender: DataSender
    private lateinit var sensorManager: SensorManager
    private lateinit var normalAuthProvider: SensorAuthProvider
    private lateinit var continuousAuthProvider: SensorAuthProvider
    private lateinit var unlockSwitchDetector: SwitchDetector
    private lateinit var continuousAuthSwitchDetector: SwitchDetector
    private lateinit var networkUtils: NetworkUtils
    private lateinit var userConfigProvider: UserConfigProvider
    private lateinit var biometricAuthAllower: BiometricAuthenticationAllower
    private val serverPostDataUrl = "https://makengineering.pythonanywhere.com/uploads/add/"
    private val serverGetProfileUrl = "https://makengineering.pythonanywhere.com/profiles/get/"
    private val serverGetContinuousAuthProfileUrl = "https://makengineering.pythonanywhere.com/profiles/get/"

    private class SimpleSensorListener(private val service: MainService) : SensorEventListener {
        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
            service.sensorDataGatherer.ProcessAccuracyChange(sensor, accuracy)
        }

        override fun onSensorChanged(event: SensorEvent?) {
            service.sensorDataGatherer.ProcessSensorEvent(event)
        }

    }

    private class EventBroadcastReceiver(private val service: MainService) : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            service.eventDataGatherer.ProcessEvent(context, intent)
        }

    }


    private class EventBroadcaster: Observable<String>() {
        fun Process(str: String) {
            Notify(str)
        }
    }

    private val eventBroadcaster = EventBroadcaster()
    private val sensorListener: SimpleSensorListener = SimpleSensorListener(this)
    var sensors: ArrayList<Sensor> = ArrayList<Sensor>(6)
    private  val eventListener: EventBroadcastReceiver = EventBroadcastReceiver(this)

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()

        makeNiceDirectories()

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startMyOwnForeground()
        else
            startMyOwnForegroundLegacy()

        //Loading globals from context
        userConfigProvider = AndroidUserConfigProvider(filesDir)
        biometricAuthAllower = BiometricAuthenticationAllower(
            numberOfAttemptsAllows = true,
            reasonBehindLockAllows = true
        )
        networkUtils = AndroidNetworkUtils(baseContext)

        val sensorfilesystemManager = AndroidInternalFileSystemManager(filesDir)
        val eventfilesystemManager = AndroidInternalFileSystemManager(filesDir)
        sensorSaver = AndroidSensorDataSaver(sensorDataGatherer, eventBroadcaster, sensorfilesystemManager)
        eventSaver = AndroidEventDataSaver(eventDataGatherer, eventBroadcaster, eventfilesystemManager)

        registerMyReceiver()
        registerScreenOffReceiver()
        registerShutdownReceiver()
        //Data collecting
        val eventFilters = IntentFilter()
        eventFilters.addAction(Intent.ACTION_SCREEN_ON)
        eventFilters.addAction(Intent.ACTION_SCREEN_OFF)
        eventFilters.addAction(Intent.ACTION_USER_PRESENT)
        eventFilters.addAction("ACTION_UNLOCKED")
        eventFilters.addAction("ACTION_UNLOCKED_BIOMETRICALLY")

        registerReceiver(eventListener, eventFilters)
        registerShutdownReceiver()
        registerMyReceiver()
        registerScreenOffReceiver()

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER))
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD))
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE))
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY))
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION))
        sensors.add(sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR))

        for (sensor in sensors) {
            sensorManager.registerListener(sensorListener, sensor, SensorManager.SENSOR_DELAY_FASTEST)
        }

        deviceId = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)

        val apiRequester = AndroidApiRequester(serverGetProfileUrl, serverGetContinuousAuthProfileUrl, serverPostDataUrl, filesDir)

        dataSender = AndroidDataSender(
            deviceId,
            sensorSaver,
            eventSaver,
            apiRequester,
            networkUtils,
            userConfigProvider,
            baseContext
        )
        try {
            dataSender.Run()

            //normalAuthProvider = AndroidSensorAuthProvider(deviceId, apiRequester, networkUtils, userConfigProvider, eventBroadcaster)
            normalAuthProvider = RestApiSensorAuthProvider(networkUtils, userConfigProvider, deviceId, apiRequester, AuthType.NORMAL)

            normalAuthProvider.Run()

            unlockSwitchDetector = AndroidSwitchDetector(sensorDataGatherer, normalAuthProvider, baseContext, biometricAuthAllower)
            //unlockSwitchDetector = MockSwitchDetector()

            unlockSwitchDetector.Run()

            continuousAuthProvider = RestApiSensorAuthProvider(networkUtils, userConfigProvider, deviceId, apiRequester, AuthType.CONTINUOUS)

            continuousAuthProvider.Run()

            continuousAuthSwitchDetector = ContinuousAuthSwitchDetector(sensorDataGatherer, continuousAuthProvider, baseContext, userConfigProvider, biometricAuthAllower)

            continuousAuthSwitchDetector.Run()
            
        }
        catch (e: Exception){
            FL.e("MainService failed: " + e.message)
            throw e
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun startMyOwnForeground() {
        val NOTIFICATION_CHANNEL_ID = "example.permanence"
        val channelName = "Background Service"
        val chan = NotificationChannel(
            NOTIFICATION_CHANNEL_ID,
            channelName,
            NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE

        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannel(chan)

        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
        val notification = notificationBuilder.setOngoing(true)
            .setSmallIcon(R.drawable.ic_secure)
            .setContentTitle("App is running in background")
            .setPriority(NotificationManager.IMPORTANCE_MIN)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
        startForeground(2, notification)
    }

    private fun startMyOwnForegroundLegacy() {
        val notificationBuilder = NotificationCompat.Builder(this, "com.service.safeservice")
        var notification =  notificationBuilder.setOngoing(true)
            .setSmallIcon(R.drawable.ic_secure)
            .setContentTitle("App is running in background")
            .setPriority(NotificationManager.IMPORTANCE_MIN)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
        startForeground(1, notification)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        startTimer()
        return START_STICKY
    }

    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    private fun startTimer() {
        timer = Timer()
        timerTask = object : TimerTask() {
            override fun run() {
                Log.i("Count", "=========  " + counter++)
            }
        }
        timer!!.schedule(timerTask, 0, 10000)
    }

    private fun stopTimerTask() {
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }

    override fun onDestroy() {
        unregisterReceiver(screenOffReceiver)
        unregisterReceiver(myReceiver)
        stopTimerTask()

        val broadcastIntent = Intent()
        broadcastIntent.action = "RestartService"
        broadcastIntent.setClass(this, Restarter::class.java)
        this.sendBroadcast(broadcastIntent)
        super.onDestroy()
    }

    private fun registerShutdownReceiver() {
        val eventFilters = IntentFilter()
        eventFilters.addAction(Intent.ACTION_SHUTDOWN)
        eventFilters.addAction(Intent.ACTION_REBOOT)
        registerReceiver(shutdownBroadcastReceiver, eventFilters)
    }

    private fun registerMyReceiver() {
        val filter = IntentFilter("CHANGE_CONFIG")
        registerReceiver(myReceiver, filter)
    }

    private fun registerScreenOffReceiver() {
        val screenStateFilter = IntentFilter(Intent.ACTION_SCREEN_OFF)
        registerReceiver(screenOffReceiver, screenStateFilter)
    }

    fun shutdown() {
        eventBroadcaster.Process("shutdown")
    }

    private fun makeNiceDirectories(){
        val sensorDir = File(filesDir, "SensorData")
        if(!sensorDir.exists()) {
            sensorDir.mkdir()
        }
        val eventDir = File(filesDir, "EventData")
        if(!eventDir.exists()) {
            eventDir.mkdir()
        }

        val fileList : MutableList<File> = mutableListOf()
        if(filesDir.listFiles()!=null) {
            filesDir.listFiles().forEach {
                run {
                    if(it.isFile)
                        fileList.add(it)
                }
            }
        }

        fileList.forEach {
            run {
                if(it.name.startsWith("Sensors")){
                    val file = File(sensorDir, it.name)
                    file.writeBytes(File(filesDir, it.name).readBytes())
                    File(filesDir, it.name).delete()
                }
                if (it.name.startsWith("Events")) {
                    val file = File(eventDir, it.name)
                    file.writeBytes(File(filesDir, it.name).readBytes())
                    File(filesDir, it.name).delete()
                }
            }
        }
    }
}