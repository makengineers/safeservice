package com.service.safeservice.CommonAbstracts

import java.io.File

interface ApiRequester {
    fun SendBinaryData(sensorsFilename: String, eventsFilename: String, extras: Map<String, String>, filePostprocesor: (File) -> Boolean)
    fun RequestProfile(extras: Map<String, String>) : BehavioralProfile?
    fun RequestContinuousAuthProfile(extras: Map<String, String>) : BehavioralProfile?
}