package com.service.safeservice.CommonAbstracts

import com.service.safeservice.Common.SensorData

interface BehavioralProfile {
    fun VerifyOnSensorData(sensorDataList: List<SensorData>) : Boolean
}