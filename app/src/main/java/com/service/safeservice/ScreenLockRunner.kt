package com.service.safeservice

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.ResolveInfo
import com.service.safeservice.CommonAbstracts.UserConfigProvider
import java.util.*

interface ScreenLockRunner {
    fun runScreenLock(context: Context, userConfigProvider: UserConfigProvider) {
        val config = userConfigProvider.GetCurrentUserConfig()
        if(config != null) {
            val pinHash = config.screenLockPINHash
            val intent = Intent()
            intent.setPackage("com.screenlock.safelock")
            val pm = context.packageManager
            val resolveInfos = pm.queryIntentActivities(intent, 0)
            Collections.sort(resolveInfos, ResolveInfo.DisplayNameComparator(pm))

            if (resolveInfos.size > 0) {
                val launchable = resolveInfos[0]
                val activity = launchable.activityInfo
                val name = ComponentName(
                    activity.applicationInfo.packageName,
                    activity.name
                )
                val i = Intent(Intent.ACTION_MAIN)
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
                i.component = name
                i.putExtra("screenLockPINHash", pinHash)
                i.putExtra("useGesture", config.useGesture)
                context.startActivity(i)
            }
        }
    }
}