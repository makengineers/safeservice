package com.service.safeservice.Events

import com.bosphere.filelogger.FL
import com.service.safeservice.Common.SystemEvent
import com.service.safeservice.CommonAbstracts.*
import com.service.safeservice.CommonAbstracts.Observable
import com.service.safeservice.CommonAbstracts.Observer
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.concurrent.locks.ReentrantLock

class AndroidEventDataSaver(dataProvider: EventDataGatherer, private val eventBroadcaster: Observable<String>?,
                            private val fileSystemManager: FileSystemManager) : EventDataSaver, Observer<SystemEvent> {
    private val dateFormat = SimpleDateFormat("yyyy.MM.dd'T'HH-mm-ss'Z'")
    private lateinit var openFile: FileOutputStream
    private var bytesWritten: Long = 0
    private var linesWritten: Long = 0
    private val maxByteFileSize: Long = 1024*100//100kB
    private val mutex = ReentrantLock()
    private val files = mutableListOf<Triple<String, Date, Boolean>>()

    inner class EventListener(private val saver: AndroidEventDataSaver): Observer<String> {
        override fun Update(data: String) {
            if(data == "shutdown")
                saver.close()
        }
    }

    private val eventListener = EventListener(this)

    @kotlin.ExperimentalUnsignedTypes
    override fun Update(data: SystemEvent) {
        mutex.lock()
        try {
            saveEventData(data)
        }
        finally {
            mutex.unlock()
        }
    }

    override fun GetChunk(): EventDataChunk {
        mutex.lock()
        try {
            removeRedundantFiles()
            reinstateFiles()
            val index: Int = files.indexOfFirst { !it.third }
            val res = files[index]
            val file = fileSystemManager.LoadFile(res.first)
            val data: ByteArray = file.readBytes()
            files.removeAt(index)
            files.add(Triple(res.first, res.second, true))

            if((files.filter { !it.third }).isEmpty())
            {
                close()
                resetFile()
            }
            return AndroidEventRawDataChunk(data, res.second)
        }
        finally {
            mutex.unlock()
        }
    }

    override fun GetChunkByFile(): EventDataChunk {
        mutex.lock()
        try {
            removeRedundantFiles()
            reinstateFiles()
            val index: Int = files.indexOfFirst { !it.third }
            val res = files[index]
            files.removeAt(index)
            files.add(Triple(res.first, res.second, true))

            if((files.filter { !it.third }).isEmpty())
            {
                close()
                resetFile()
            }
            return AndroidEventFileDataChunk(res.first, res.second)
        }
        finally {
            mutex.unlock()
        }
    }

    override fun IsChunkAvailable(): Boolean {
        return (files.filter { !it.third }).isNotEmpty()
    }

    private fun resetFile()
    {
        val date = Date()
        val filename = "Events" + dateFormat.format(date)
        openFile = fileSystemManager.OpenFile(filename)
        bytesWritten = 0
        linesWritten = 0
        files.add(Triple(filename, date, false))
        FL.i("Next event file created. Current number of event data files: ${files.size}")
    }


    private fun getDataFilename() : String {
        val actData = Date()
        return dateFormat.format(actData)
    }

    init {
        val directoryExists = fileSystemManager.CreateDirectory("EventData")
        if(directoryExists){
            fileSystemManager.ChangeWorkingDirectory("EventData")
            val fileList = fileSystemManager.ListFiles()
            fileList.forEach {
                run {
                    val filename = it.name
                    val dateOfCreation = dateFormat.parse(filename.substring(7, filename.length))
                    files.add(Triple(filename, dateOfCreation, false))
                }
            }
        }
        FL.i("Event files sending queue initialized, size: ${files.size}")
        resetFile()
    }

    @kotlin.ExperimentalUnsignedTypes
    private fun saveEventData(data: SystemEvent) {
        val rawData: ByteArray = data.ToBytes().toByteArray()
        openFile.write(rawData)
        bytesWritten += rawData.size
        linesWritten += 1
        checkFile()
    }

    private fun checkFile() {
        if(bytesWritten >= maxByteFileSize)
        {
            close()
            resetFile()
        }
    }

    private fun close() {
        openFile.close()
    }

    private fun removeRedundantFiles() {
        val sizeBefore = files.size
        files.removeAll { it.third && !fileSystemManager.ContainsFile(it.first)}
        FL.i("Already sent event files removed from sending queue. Before: $sizeBefore, After: ${files.size}")
        var log = "Current event queue state:\n\t"
        fileSystemManager.ListFiles().forEach {
            val sizeInB = it.length()
            log += it.name + " - $sizeInB B\n\t"
        }
        FL.i(log.substring(0,log.length-2))
    }

    private fun reinstateFiles(){
        for(i in 0 until files.size){
            if(files[i].third) {
                val file = files[i]
                files.removeAt(i)
                files.add(Triple(file.first, file.second, false))
            }
        }
    }

    init {
        dataProvider.Subscribe(this)
    }

    init {
        eventBroadcaster?.Subscribe(eventListener)
    }
}