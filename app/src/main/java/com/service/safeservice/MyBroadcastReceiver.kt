package com.service.safeservice

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import com.google.gson.Gson
import com.service.safeservice.Configs.UserConfig
import java.io.File

private const val TAG = "MyBroadcastReceiver"

class MyBroadcastReceiver : BroadcastReceiver(), ScreenLockRunner {
    override fun onReceive(context: Context, intent: Intent) {

        val screenLockPINHash  = intent.getStringExtra("screenLockPINHash")
        val algorithmSensitivity = intent.getStringExtra("algorithmSensitivity")
        val enableLock =  intent.getBooleanExtra("enableLock", true)
        val enableMobileDataUsage = intent.getBooleanExtra("enableMobileDataUsage", false)
        val enableUnlock =  intent.getBooleanExtra("enableUnlock", true)
        val useGesture = intent.getBooleanExtra("useGesture", false)

        val gson = Gson()
        val config = UserConfig(
            screenLockPINHash = screenLockPINHash, algorithmSensitivity = algorithmSensitivity,
            enableLock = enableLock, enableMobileDataUsage = enableMobileDataUsage,
            enableUnlock = enableUnlock, useGesture = useGesture
        )
        val jsonString = gson.toJson(config)

        val file = File(context.filesDir, "config.json")
        file.writeText(jsonString) //overwrites as it should

        val newIntent = Intent()
        newIntent.action = "CONFIG_CHANGED"
        context.sendBroadcast(newIntent)
    }
}