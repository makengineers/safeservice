package com.service.safeservice.Sensors

import com.bosphere.filelogger.FL
import com.service.safeservice.Common.SensorData
import com.service.safeservice.CommonAbstracts.*
import com.service.safeservice.CommonAbstracts.Observable
import com.service.safeservice.CommonAbstracts.Observer
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.locks.ReentrantLock


class AndroidSensorDataSaver(dataProvider: SensorDataGatherer, private val eventBroadcaster: Observable<String>?,
                             private val fileSystemManager: FileSystemManager)
    : Observer<SensorData>, SensorDataSaver {
    private val dateFormat = SimpleDateFormat("yyyy.MM.dd'T'HH-mm-ss'Z'")
    private lateinit var openFile: FileOutputStream
    private var bytesWritten: Long = 0
    private var linesWritten: Long = 0
    private val maxByteFileSize: Long = 1024*1024*4//4MB
    private val files = mutableListOf<Triple<String, Date, Boolean>>()
    private val mutex = ReentrantLock()

    inner class EventListener(private val saver: AndroidSensorDataSaver): Observer<String> {
        override fun Update(event: String) {
            if(event == "shutdown")
                saver.close()
        }
    }

    private val eventListener = EventListener(this)

    override fun Update(data: SensorData) {
        mutex.lock()
        try {
            saveSensorData(data)
        }
        finally {
            mutex.unlock()
        }
    }

    override fun GetChunk(): SensorDataChunk {
        mutex.lock()
        try {
            removeRedundantFiles()
            reinstateFiles()
            val index: Int = files.indexOfFirst { !it.third }
            val res = files[index]
            val file = fileSystemManager.LoadFile(res.first)
            val data: ByteArray = file.readBytes()
            files.removeAt(index)
            files.add(Triple(res.first, res.second, true))

            if((files.filter { !it.third }).isEmpty())
            {
                close()
                resetFile()
            }
            return AndroidSensorRawDataChunk(data, res.second)
        }
        finally {
            mutex.unlock()
        }
    }

    override fun GetChunkByFile(): SensorDataChunk {
        mutex.lock()
        try {
            removeRedundantFiles()
            reinstateFiles()
            val index: Int = files.indexOfFirst { !it.third }
            val res = files[index]
            files.removeAt(index)
            files.add(Triple(res.first, res.second, true))

            if((files.filter { !it.third }).isEmpty())
            {
                close()
                resetFile()
            }
            return AndroidSensorFileDataChunk(res.first, res.second)
        }
        finally {
            mutex.unlock()
        }
    }

    override fun IsChunkAvailable(): Boolean {
        return (files.filter { !it.third }).size > 1
    }

    private fun resetFile()
    {
        val date = Date()
        val filename = "Sensors"+dateFormat.format(date)
        openFile = fileSystemManager.OpenFile(filename)
        bytesWritten = 0
        linesWritten = 0
        files.add(Triple(filename, date, false))
        FL.i("Next sensor file created. Current number of sensor data files: ${files.size}")
    }

    private fun getDataFilename() : String {
        val actData = Date()
        return dateFormat.format(actData)
    }

    init {
        val directoryExists = fileSystemManager.CreateDirectory("SensorData")
        if(directoryExists){
            fileSystemManager.ChangeWorkingDirectory("SensorData")
            val fileList = fileSystemManager.ListFiles()
            fileList.forEach {
                run {
                    val filename = it.name
                    val dateOfCreation = dateFormat.parse(filename.substring(7, filename.length))
                    files.add(Triple(filename, dateOfCreation, false))
                }
            }
        }
        FL.i("Sensor files sending queue initialized, size: ${files.size}")
        resetFile()
    }

    private fun saveSensorData(data: SensorData) {
        val rawData: ByteArray = data.ToBytes().toByteArray()
        openFile.write(rawData)
        bytesWritten += rawData.size
        linesWritten += 1
        checkFile()
    }

    private fun checkFile() {
        if(bytesWritten >= maxByteFileSize)
        {
            close()
            resetFile()
        }
    }

    private fun close() {
        openFile.close()
    }

    private fun removeRedundantFiles() {
        val sizeBefore = files.size
        files.removeAll { it.third && !fileSystemManager.ContainsFile(it.first)}
        FL.i("Already sent sensor files removed from sending queue. Before: $sizeBefore, After: ${files.size}")
        var log = "Current sensor queue state:\n\t"
        fileSystemManager.ListFiles().forEach {
            val sizeInMB = it.length().toFloat()/(1024*1024)
            log += it.name + " - " + "%.2f".format(sizeInMB) + " MB\n\t"
        }
        FL.i(log.substring(0,log.length-2))
    }

    private fun reinstateFiles(){
        for(i in 0 until files.size){
            if(files[i].third) {
                val file = files[i]
                files.removeAt(i)
                files.add(Triple(file.first, file.second, false))
            }
        }
    }

    init {
        dataProvider.Subscribe(this)
    }

    init {
        eventBroadcaster?.Subscribe(eventListener)
    }
}