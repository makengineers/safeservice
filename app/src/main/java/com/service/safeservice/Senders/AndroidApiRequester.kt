package com.service.safeservice.Senders

import com.bosphere.filelogger.FL
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FileDataPart
import com.google.gson.Gson
import com.service.safeservice.Auth.RandBehavioralProfile
import com.service.safeservice.Common.AuthorizeGetApiResponse
import com.service.safeservice.Common.DataPostApiResponse
import com.service.safeservice.Common.ProfileGetApiResponse
import com.service.safeservice.CommonAbstracts.ApiRequester
import com.service.safeservice.CommonAbstracts.BehavioralProfile
import java.io.File
import java.util.concurrent.locks.ReentrantLock


class AndroidApiRequester(private val serverProfileGetUrl: String, private val serverContinuousAuthProfileGetUrl: String,
                          private val serverDataPostUrl: String, private val filesDir: File) : ApiRequester {

    private var deviceToken: String = "deviceTokenNotReceived"
    private val app_token: String = "944d5555-48bf-48b2-b690-0065b9ba0bdd"
    private val mutex = ReentrantLock()
    private val authorizeUrl = "https://makengineering.pythonanywhere.com/profiles/authresult/"
    private val continuousAuthorizeUrl = "https://makengineering.pythonanywhere.com/profiles/authresult/"

    override fun SendBinaryData(sensorsFilename: String, eventsFilename: String, extras: Map<String, String>,
                                filePostprocesor: (File) -> Boolean) {
        FL.i("Sending: files: $sensorsFilename, $eventsFilename")
        mutex.lock()
        try {
            val tokenFile = File(filesDir, "receivedDeviceToken.txt")
            if(tokenFile.exists()) {
                if(tokenFile.canRead()){
                    deviceToken = tokenFile.readText()
                }
            }
            FL.i("Sending files with device token: $deviceToken")
            val formData : ArrayList<Pair<String, String>> = ArrayList(5)
            for ((k,v) in extras) {
                formData.add(Pair(k,v))
            }
            formData.add(Pair("device_token", deviceToken))
            formData.add(Pair("app_token", app_token))

            val sensorDir = File(filesDir, "SensorData")
            val eventDir = File(filesDir, "EventData")
            val sensorFile = File(sensorDir, sensorsFilename)
            val eventFile = File(eventDir, eventsFilename)

            Fuel.upload(serverDataPostUrl, parameters = formData)
                .add (
                    FileDataPart(sensorFile, name = "sensor_file", filename = sensorsFilename),
                    FileDataPart(eventFile, name = "event_file", filename = eventsFilename)
                )
                .response { _, _, result ->
                    val (bytes, error) = result
                    if(error != null) {
                        FL.e("Fuel error", error.message.toString())
                    }
                    if (bytes != null) {
                        val gson = Gson()

                        val response = gson.fromJson(String(bytes), DataPostApiResponse::class.java)

                        var log = "Server response received for: $sensorsFilename, $eventsFilename\n\t"

                        val file = File(filesDir, "receivedDeviceToken.txt")
                        if(response.device_token != null) {
                            file.writeText(response.device_token)
                            log += "Received device token: ${response.device_token}"
                        }
                        if(response.error != null) {
                            FL.e("Server error",log + "Message: ${response.error}")
                        }
                        else {
                            FL.i("Files sent successfully!\n\t$log")
                            val sensorFilePostprocessed = filePostprocesor(sensorFile)
                            val eventFilePostprocessed = filePostprocesor(eventFile)
                            FL.i("$sensorsFilename removed: $sensorFilePostprocessed")
                            FL.i("$eventsFilename removed: $eventFilePostprocessed")
                        }
                    }
                }
        }
        finally {
            mutex.unlock()
        }

    }

    override fun RequestProfile(extras: Map<String, String>) : BehavioralProfile? {
        FL.i("Behavioral profile request sending...")
        mutex.lock()
        try {
            val bodyData : ArrayList<Pair<String, String>> = ArrayList(3)
            for ((k,v) in extras) {
                bodyData.add(Pair(k,v))
            }
            bodyData.add(Pair("device_token", deviceToken))
            bodyData.add(Pair("app_token", app_token))
            bodyData.add(Pair("profile_type", "UNLOCK"))

            var profile: BehavioralProfile? = null
            val (_,_,result) = Fuel.get(serverProfileGetUrl, parameters = bodyData).responseString()

            result.let {
                val (bytes, error) = it
                if (error != null) {
                    FL.e("Fuel error", error.message.toString())
                }
                if (bytes != null) {
                    val gson = Gson()

                    val response = gson.fromJson(bytes, ProfileGetApiResponse::class.java)

                    if (response.error != null) {
                        FL.e("Server error","Message: ${response.error}")
                        return null
                    }
                    if(response.profile_ready != null){
                        FL.i("Received profile is ready: ${response.profile_ready}")
                        if (response.profile_ready) {
                            FL.i("Result profile created ${response.creation_date} is ready")
                        }
                    }
                    else FL.w("Information about profile availability is null")
                    //TEMP TODO
                    profile = RandBehavioralProfile()
                }
            }
            return profile
        } finally {
            mutex.unlock()
        }
    }

    override fun RequestContinuousAuthProfile(extras: Map<String, String>) : BehavioralProfile? {
        FL.i("Behavioral profile request sending...")
        mutex.lock()
        try {
            val bodyData : ArrayList<Pair<String, String>> = ArrayList(3)
            for ((k,v) in extras) {
                bodyData.add(Pair(k,v))
            }
            bodyData.add(Pair("device_token", deviceToken))
            bodyData.add(Pair("app_token", app_token))
            bodyData.add(Pair("profile_type", "CONTINUOUS"))

            var profile: BehavioralProfile? = null
            val (_,_,result) = Fuel.get(serverContinuousAuthProfileGetUrl, parameters = bodyData).responseString()

            result.let {
                val (bytes, error) = it
                if (error != null) {
                    FL.e("Fuel error", error.message.toString())
                }
                if (bytes != null) {
                    val gson = Gson()

                    val response = gson.fromJson(bytes, ProfileGetApiResponse::class.java)

                    if (response.error != null) {
                        FL.e("Server error","Message: ${response.error}")
                        return null
                    }
                    if(response.profile_ready != null){
                        FL.i("Received profile is ready: ${response.profile_ready}")
                        if (response.profile_ready) {
                            FL.i("Result profile created ${response.creation_date} is ready")
                        }
                    }
                    else FL.w("Information about profile availability is null")
                    //TEMP TODO
                    profile = RandBehavioralProfile()
                }
            }
            return profile
        } finally {
            mutex.unlock()
        }
    }

    fun Authorize(data: List<Pair<Long, List<Float>>>, extras: Map<String, String>) : Float {
        FL.i("Authorize request sending...")
        mutex.lock()
        try {
            val tokenFile = File(filesDir, "receivedDeviceToken.txt")
            if(tokenFile.exists()) {
                if(tokenFile.canRead()){
                    deviceToken = tokenFile.readText()
                }
            }
            val formData : ArrayList<Pair<String, String>> = ArrayList(5)
            for ((k,v) in extras) {
                formData.add(Pair(k,v))
            }
            formData.add(Pair("device_token", deviceToken))
            formData.add(Pair("app_token", app_token))
            formData.add(Pair("profile_type", "UNLOCK"))

            val gson = Gson()

            val list = mutableListOf<List<Any>>()
            for (it in data) {
                val l = mutableListOf<Any>()
                l.add(it.first)
                for (it2 in it.second) {
                    l.add(it2)
                }
                list.add(l)
            }
            formData.add(Pair("sensor_data", gson.toJson(list)))

            //val (_,_,result) = Fuel.get(authorizeUrl, parameters = bodyData).responseString()
            val (_,_,result) = Fuel.upload(authorizeUrl, parameters = formData).responseString()

            result.let {
                val (bytes, error) = it
                if (error != null) {
                    FL.e("Fuel error", error.message.toString())
                }
                if (bytes != null) {


                    val response = gson.fromJson(bytes, AuthorizeGetApiResponse::class.java)

                    if (response.error != null) {
                        FL.e("Server error","Message: ${response.error}")
                        return 0F
                    }

                    if(response.profile_ready != null){
                        FL.i("Received profile is ready: ${response.profile_ready}")
                        if (!response.profile_ready) {
                            FL.i("Cannot authorize - profile not ready")
                            return 0F
                        }
                    }

                    if(response.matching_user_probability != null){
                        FL.i("Received authentication result: ${response.matching_user_probability}")
                        return response.matching_user_probability
                    }
                    else FL.w("Information about authorization result is unavailable")
                    return 0F
                }
            }
        } finally {
            mutex.unlock()
        }
        return 0F
    }

    fun AuthorizeContinuous(data: List<Pair<Long, List<Float>>>, extras: Map<String, String>) : Float {
        FL.i("Authorize Continuous request sending...")
        mutex.lock()
        try {
            val tokenFile = File(filesDir, "receivedDeviceToken.txt")
            if(tokenFile.exists()) {
                if(tokenFile.canRead()){
                    deviceToken = tokenFile.readText()
                }
            }
            val formData : ArrayList<Pair<String, String>> = ArrayList(5)
            for ((k,v) in extras) {
                formData.add(Pair(k,v))
            }
            formData.add(Pair("device_token", deviceToken))
            formData.add(Pair("app_token", app_token))
            formData.add(Pair("profile_type", "CONTINUOUS"))

            val gson = Gson()

            val list = mutableListOf<List<Any>>()
            for (it in data) {
                val l = mutableListOf<Any>()
                l.add(it.first)
                for (it2 in it.second) {
                    l.add(it2)
                }
                list.add(l)
            }
            formData.add(Pair("sensor_data", gson.toJson(list)))

            //val (_,_,result) = Fuel.get(authorizeUrl, parameters = bodyData).responseString()
            val (_,_,result) = Fuel.upload(continuousAuthorizeUrl, parameters = formData).responseString()

            result.let {
                val (bytes, error) = it
                if (error != null) {
                    FL.e("Fuel error", error.message.toString())
                }
                if (bytes != null) {


                    val response = gson.fromJson(bytes, AuthorizeGetApiResponse::class.java)

                    if (response.error != null) {
                        FL.e("Server error","Message: ${response.error}")
                        return 0F
                    }

                    if(response.profile_ready != null){
                        FL.i("Received profile is ready: ${response.profile_ready}")
                        if (!response.profile_ready) {
                            FL.i("Cannot authorize - profile not ready")
                            return 0F
                        }
                    }

                    if(response.matching_user_probability != null){
                        FL.i("Received authentication result: ${response.matching_user_probability}")
                        return response.matching_user_probability
                    }
                    else FL.w("Information about authorization result is unavailable")
                    return 0F
                }
            }
        } finally {
            mutex.unlock()
        }
        return 0F
    }
}