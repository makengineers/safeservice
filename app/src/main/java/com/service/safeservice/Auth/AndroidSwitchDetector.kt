package com.service.safeservice.Auth

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.util.Log
import com.service.safeservice.CommonAbstracts.SensorAuthProvider
import com.service.safeservice.CommonAbstracts.SensorDataGatherer
import com.service.safeservice.CommonAbstracts.SwitchDetector
import com.service.safeservice.Common.BiometricAuthenticationAllower
import java.util.*

class AndroidSwitchDetector(private val sensorDataGatherer: SensorDataGatherer, private val authProvider: SensorAuthProvider,
                            private val context: Context, private val biometricAuthAllower : BiometricAuthenticationAllower
)
    : SwitchDetector, BroadcastReceiver() {
    private val delay: Long = 1000 //1s
    private val eventsDelay: Int = 4000 //4s (3s before event's occurrence)
    private lateinit var timer: Timer
    override fun Run() {
        val eventFilters = IntentFilter()
        eventFilters.addAction(Intent.ACTION_USER_PRESENT)
        eventFilters.addAction("ACTION_PHONE_UNLOCKED")
        eventFilters.addAction("ACTION_UNLOCKED_BIOMETRICALLY")
        context.registerReceiver(this, eventFilters)
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if(intent!!.action == "ACTION_PHONE_UNLOCKED" || intent.action == "ACTION_UNLOCKED_BIOMETRICALLY") {
            biometricAuthAllower.ChangeWhetherNumberOfAttemptsAllows(true)
            biometricAuthAllower.ChangeWhetherReasonBehindLockAllows(true)
            return
        }
        timer = Timer()
        val task = object : TimerTask() {
            override fun run() {
                val newIntent = Intent()
                newIntent.action = "Authentication_result"

                val authResult = authProvider.Authorize(sensorDataGatherer.GetLastEventsInTime(eventsDelay))

                if(biometricAuthAllower.IsAllowed()) {
                    when {
                        authResult == null -> {
                            Log.i("AUTH-RESULT", "No biometric authorization required")
                        }
                        authResult -> {
                            Log.i("AUTH-RESULT", "Biometric authorization succeeded")
                            newIntent.putExtra("AuthenticationSucceeded", true)
                            context!!.sendBroadcast(newIntent)

                            val biometricAuthenticationIntent = Intent()
                            biometricAuthenticationIntent.action = "ACTION_UNLOCKED_BIOMETRICALLY"
                            context.sendBroadcast(biometricAuthenticationIntent)
                        }
                        !authResult -> {
                            Log.i("AUTH-RESULT", "Biometric authorization failed")
                            newIntent.putExtra("AuthenticationSucceeded", false)
                            context!!.sendBroadcast(newIntent)
                        }
                    }
                }
                else {
                    Log.i("AUTH-RESULT", "Biometric authorization not allowed")
                }
                biometricAuthAllower.ChangeWhetherNumberOfAttemptsAllows(false)
            }
        }
        timer.schedule(task, delay)
    }

}