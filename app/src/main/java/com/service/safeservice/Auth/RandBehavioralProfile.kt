package com.service.safeservice.Auth

import com.service.safeservice.CommonAbstracts.BehavioralProfile
import com.service.safeservice.Common.SensorData
import kotlin.random.Random
import kotlin.random.nextInt

class RandBehavioralProfile : BehavioralProfile {
    private val seed: Int = 7
    private val rand: Random = Random(seed)
    override fun VerifyOnSensorData(sensorDataList: List<SensorData>): Boolean {
        return rand.nextInt(IntRange(0,1)) == 1
    }

}