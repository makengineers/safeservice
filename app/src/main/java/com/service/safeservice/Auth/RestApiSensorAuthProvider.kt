package com.service.safeservice.Auth

import com.service.safeservice.Common.AuthType
import com.service.safeservice.Common.SensorData
import com.service.safeservice.CommonAbstracts.NetworkUtils
import com.service.safeservice.CommonAbstracts.SensorAuthProvider
import com.service.safeservice.CommonAbstracts.UserConfigProvider
import com.service.safeservice.Senders.AndroidApiRequester

class RestApiSensorAuthProvider(private val networkUtils: NetworkUtils,
                                private  val userConfigProvider: UserConfigProvider,
                                private val deviceId: String,
                                private val requester: AndroidApiRequester,
                                private val authType: AuthType)
    : SensorAuthProvider {

    private val allowMobile : Boolean get() {
        val config = userConfigProvider.GetCurrentUserConfig()
        return config?.enableMobileDataUsage ?: false
    }
    override fun Run() {
    }

    override fun Authorize(data: List<SensorData>): Boolean? {
        val config = userConfigProvider.GetCurrentUserConfig()
        if(config == null || !config.enableUnlock)
            return null

        val converted = mutableListOf<Pair<Long, List<Float>>>()

        for (it in data) {
            converted.add(Pair(it.timestamp, it.ToVector().toList()))
        }

        if(!canSend())
            return false


        val authResult =
            if(authType == AuthType.NORMAL) {
                requester.Authorize(converted, mapOf<String, String>("device_id" to deviceId))
            }
            else {
                requester.AuthorizeContinuous(converted, mapOf<String, String>("device_id" to deviceId))
            }

        return when (config.algorithmSensitivity) {
            "Low" -> {
                authResult>0.5F
            }
            "Medium" -> {
                authResult>0.75F
            }
            "High" -> {
                authResult>0.9F
            }
            else -> null
        }
    }

    private fun canSend() : Boolean {
        val mobileAvailable: Boolean = networkUtils.isMobileConnected()
        val wifiAvailable: Boolean = networkUtils.isWifiConnected()
        return wifiAvailable || (mobileAvailable && allowMobile)
    }
}