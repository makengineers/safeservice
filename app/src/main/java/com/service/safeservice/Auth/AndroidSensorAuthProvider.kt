package com.service.safeservice.Auth

import android.util.Log
import com.service.safeservice.Common.AuthType
import com.service.safeservice.Common.SensorData
import com.service.safeservice.CommonAbstracts.*
import com.service.safeservice.CommonAbstracts.Observable
import com.service.safeservice.CommonAbstracts.Observer
import java.io.FileNotFoundException
import java.util.*

class AndroidSensorAuthProvider(private val deviceId: String, private val requester: ApiRequester,
    private val networkUtils: NetworkUtils, private val userConfigProvider: UserConfigProvider,
    private val eventBroadcaster: Observable<String>?, private val authType: AuthType)
    : SensorAuthProvider {
    private var profile: BehavioralProfile? = null
    //private val filesDir = context.filesDir
    //private val profileFilename = "profile.obj"
    private val interval: Long = 1000 * 3600 * 2 //2h
    private lateinit var timer : Timer
    private val allowMobile : Boolean get() {
        val config = userConfigProvider.GetCurrentUserConfig()
        return config?.enableMobileDataUsage ?: false
    }

    inner class EventListener(private val provider: AndroidSensorAuthProvider): Observer<String> {
        override fun Update(data: String) {
            if(data == "shutdown")
                provider.close()
        }
    }

    private val eventListener = EventListener(this)


    override fun Run() {
        try {
//            val file = File(filesDir, profileFilename)
//            val stream = file.inputStream()
//            ObjectInputStream(stream).use{
//                val readObj = it.readObject()
//                if(readObj is BehavioralProfile)
//                    profile = readObj
//                else
//                    profile = null
//            }
//            stream.close()
            profile = RandBehavioralProfile()
        }
        catch(e: FileNotFoundException){
            Log.i("AUTH-PROVIDER","File with profile doesn't exists")
        }

        timer = Timer()
        val task = object : TimerTask() {
            override fun run() {
                    requestProfile()
                }
            }
        timer.schedule(task, this.interval, this.interval)
    }

    private fun close() {
//        val file = File(filesDir, profileFilename)
//        val stream = file.outputStream()
//        ObjectOutputStream(stream).use{ it.writeObject(profile)}
//        stream.close()
    }

    private fun requestProfile() {

        if(!canSend())
            return

        val new_profile =
            if(authType == AuthType.NORMAL) {
                requester.RequestProfile( mapOf<String, String>(
                    "device_id" to deviceId
                ))
            }
            else {
                requester.RequestContinuousAuthProfile( mapOf<String, String>(
                    "device_id" to deviceId
                ))
            }

        if(new_profile != null) {
            profile = new_profile
        }
    }

    private fun canSend() : Boolean {
        val mobileAvailable: Boolean = networkUtils.isMobileConnected()
        val wifiAvailable: Boolean = networkUtils.isWifiConnected()
        return wifiAvailable || (mobileAvailable && allowMobile)
    }


    override fun Authorize(data: List<SensorData>): Boolean {
        return profile?.VerifyOnSensorData(data) ?: false
    }

    init {
        eventBroadcaster?.Subscribe(eventListener)
    }
}