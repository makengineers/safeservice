package com.service.safeservice.Configs

data class UserConfig(val screenLockPINHash: String?, val algorithmSensitivity: String?, val enableLock: Boolean,
                       val enableMobileDataUsage: Boolean, val enableUnlock: Boolean, val useGesture: Boolean)