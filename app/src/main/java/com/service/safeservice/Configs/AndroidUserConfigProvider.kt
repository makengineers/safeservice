package com.service.safeservice.Configs

import com.google.gson.Gson
import com.service.safeservice.CommonAbstracts.UserConfigProvider
import java.io.File

class AndroidUserConfigProvider(private val filesDir: File) : UserConfigProvider {
    override fun GetCurrentUserConfig() : UserConfig? {
        val file = File(filesDir, "config.json")
        val gson = Gson()
        if(file.exists() && file.canRead()) {
            val instance = gson.fromJson(file.readText(), UserConfig::class.java)
            return instance
        }
        return null
    }
}