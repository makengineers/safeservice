package com.service.safeservice

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.bosphere.filelogger.FL
import com.bosphere.filelogger.FLConfig
import com.bosphere.filelogger.FLConst
import com.service.safeservice.Configs.AndroidUserConfigProvider
import java.io.File


class MainActivity : AppCompatActivity(), ScreenLockRunner  {
    var mServiceIntent: Intent? = null
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        FL.init(
            FLConfig.Builder(this)
                .minLevel(FLConst.Level.V)
                .logToFile(true)
                .dir(File(baseContext.getExternalFilesDir(null),"logs"))
                .retentionPolicy(FLConst.RetentionPolicy.TOTAL_SIZE)
                .maxTotalSize(1024*1024*10)
                .build())
        FL.setEnabled(true)

        try {
            val userConfigProvider = AndroidUserConfigProvider(baseContext.filesDir)
            if (userConfigProvider != null) {

                runScreenLock(this, userConfigProvider)
            }

            mServiceIntent = Intent(this, MainService::class.java)
            if (!isMyServiceRunning(MainService::class.java)) {
                startService(mServiceIntent)
            }

            val intent = Intent(this, MainService::class.java)
            startService(intent)
            finish()
        }
        catch(e: Exception){
            FL.e("MainActivity failed: " + e.message)
            throw e
        }
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                Log.i("Service status", "Running")
                return true
            }
        }
        Log.i("Service status", "Not running")
        return false
    }

}